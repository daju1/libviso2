
#ifdef libviso2_EXPORTS
#define LIBVISO2_API __declspec(dllexport)
#else
#define LIBVISO2_API __declspec(dllimport)
#endif



#include <viso_mono.h>
#include <viso_stereo.h>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/opencv.hpp>

typedef void *HANDLE;
#define USE_CAM 0

// This class is exported from the MyTest.dll
class LIBVISO2_API CLibVisoApi {
public:
  CLibVisoApi();
  ~CLibVisoApi();

  bool Init();
  bool CalcCurentMatrix();
  Matrix pose;
  Matrix motion;
  Matrix inv_motion;

  std::vector<double> tr_delta;
private:
  HANDLE hEvent;
#if USE_CAM
  cv::VideoCapture cap;
  // init visual odometry
  VisualOdometryMono viso;
  VisualOdometryMono::parameters param;
#else
  std::string dir;
  int iFrame;

  // init visual odometry
  VisualOdometryStereo viso;
  VisualOdometryStereo::parameters param;
#endif


  
  // current pose (this matrix transforms a point from the current
  // frame's camera coordinates to the first frame's camera coordinates)


  int32_t width;
  int32_t height;

  cv::Mat left_img, right_img;

  uint8_t* left_img_data;
  uint8_t* right_img_data;

};

extern "C"
{
  LIBVISO2_API CLibVisoApi* CreateLibVisoApi();
  LIBVISO2_API void DeleteLibVisoApi(CLibVisoApi* api);
  LIBVISO2_API void Init(CLibVisoApi* api);
  LIBVISO2_API bool CalcCurentMatrix(CLibVisoApi* api);
  LIBVISO2_API _FLOAT* GetPoseMatrix(CLibVisoApi* api);
  LIBVISO2_API _FLOAT* GetMotionMatrix(CLibVisoApi* api);
  LIBVISO2_API _FLOAT* GetInvMotionMatrix(CLibVisoApi* api);
  LIBVISO2_API _FLOAT* GetTranslationDelta(CLibVisoApi* api);
}
