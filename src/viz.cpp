#include "viso.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2\opencv.hpp>
#include <opencv2/viz.hpp>

void initViz(cv::viz::Viz3d& viz_)
{
    viz_.setBackgroundMeshLab();
    viz_.showWidget("coo", cv::viz::WCoordinateSystem(10));

    /// Construct a cube widget
    cv::viz::WCube cube_widget(cv::Point3d(5, 5, 0.0), cv::Point3d(0.0, 0.0, -5), true, cv::viz::Color::red());
    cube_widget.setRenderingProperty(cv::viz::LINE_WIDTH, 4.0);

    const int N = 100;
    const int n = 10;
    char str[100];

    for (int i = -N; i < N; i+=10)
    {
       cv::viz::WLine line(cv::Point3d(i, 0.0, -N), cv::Point3d(i, 0.0, N));
       sprintf(str, "iline_%d", i);
       viz_.showWidget(str, line);
    }

    for ( int j = -N; j < N; j+=10)
    {
       cv::viz::WLine line(cv::Point3d(-N, 0.0, j), cv::Point3d(N, 0.0, j));
       sprintf(str, "jline_%d", j);
       viz_.showWidget(str, line);
    }

    /// Display widget (update if already displayed)
    viz_.showWidget("Cube Widget", cube_widget);

    /// Rodrigues vector
    viz_.spinOnce();
}

void updateViz(cv::viz::Viz3d viz_, const Matrix& m_pose)
{
   static cv::Affine3d::Vec3 t_prev;
   static int n = 0;


    cv::Matx<_FLOAT, 4, 4> mat4;

    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            mat4(i,j) = m_pose.val[i][j];
        }
    }

    cv::Affine3d pose(mat4);
    cv::Affine3d::Vec3 t = pose.translation();

    cv::viz::WLine line(t_prev, t);

    char str[100];
    sprintf(str, "nline_%d", n);
    viz_.showWidget(str, line);

    viz_.setWidgetPose("Cube Widget", pose);
    //pose.matrix.val[4] -= 10;
    //viz_.setViewerPose(pose);
    viz_.spinOnce(1, true);

    ++n;
    t_prev = t;

}

