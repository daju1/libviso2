#include <stdarg.h>
#include <stdio.h>

void log(const char* format, ...)
{
	FILE * f = fopen("libviso.log","at");
	if (!f)
		return;


	va_list argList;

	va_start(argList, format);
	fprintf(f, format, argList);
	fprintf(f, "\n");

	va_end(argList);

	fclose(f);
}