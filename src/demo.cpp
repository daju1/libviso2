/*
Copyright 2012. All rights reserved.
Institute of Measurement and Control Systems
Karlsruhe Institute of Technology, Germany

This file is part of libviso2.
Authors: Andreas Geiger

libviso2 is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or any later version.

libviso2 is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
libviso2; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301, USA 
*/

/*
  Documented C++ sample code of stereo visual odometry (modify to your needs)
  To run this demonstration, download the Karlsruhe dataset sequence
  '2010_03_09_drive_0019' from: www.cvlibs.net!
  Usage: ./viso2 path/to/sequence/2010_03_09_drive_0019
*/

#include <iostream>
#include <string>
#include <vector>
//#include <stdint.h>

#include <viso_stereo.h>

#if 0
#include <png++/png.hpp>
#else
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2\opencv.hpp>
#include <opencv2/viz.hpp>
#endif

cv::viz::Viz3d viz_("abc");
void initViz(cv::viz::Viz3d& viz_);
void updateViz(cv::viz::Viz3d viz_, const Matrix& m_pose);

#include "tcp_client.h"

using namespace std;

int test_two_cameras()
{
	CvCapture *capture0 = cvCaptureFromCAM(0);
	CvCapture *capture1 = cvCaptureFromCAM(1);

	if (!capture0) return 1;
	if (!capture1) return 1;
	cvNamedWindow("Video0");
	cvNamedWindow("Video1");

	while (true) {
		//grab and retrieve each frames of the video sequentially 
		IplImage* frame0 = cvQueryFrame(capture0);
		IplImage* frame1 = cvQueryFrame(capture1);

		if (!frame0 || !frame1) break;

		cvShowImage("Video0", frame0);
		cvShowImage("Video1", frame1);

		//wait for 40 milliseconds
		int c = cvWaitKey(40);

		//exit the loop if user press "Esc" key  (ASCII value of "Esc" is 27) 
		if ((char)c == 27) break;
	}

	cvReleaseCapture(&capture0);
	cvReleaseCapture(&capture1);


	return 0;
}
#if 0
void PeopleCounter::saveConfig()
{
   INFOFMT("saveConfig(%s)", % config_file.c_str());

   CvFileStorage* fs = cvOpenFileStorage(config_file.c_str(), 0, CV_STORAGE_WRITE);

   if (0 == fs)
   {
      INFOFMT("savingConfig(%s) failed", % config_file.c_str());
      return;
   }
   cvWriteComment(fs, "", 0);
   cvWriteComment(fs, "��������� ��������� classical KLT tracker", 0);
   cvWriteComment(fs, "������������ �� TrackFeatures �������� ���������� KLT", 0);
   cvWriteInt(fs, "track_features_algorithm", track_features_algorithm);

   cvWriteComment(fs, "�������� max_residue ������� KLTCreateTrackingContext() - ����������� ���������� ������� � ��������� klt ��������", 0);
   cvWriteReal(fs, "max_residue", max_residue);

   cvWriteComment(fs, "", 0);
   cvWriteComment(fs, "��������� csv", 0);
   cvWriteComment(fs, "���������� ��� ���������� ����������� � csv", 0);
   cvWriteString(fs, "log_files_directory", log_files_directory.c_str());
   cvWriteComment(fs, "����� ���� � ���������� ������ ������ � ���������� csv �����", 0);

   cvReleaseFileStorage(&fs);
}

void PeopleCounter::loadConfig()
{
   CvFileStorage* fs = cvOpenFileStorage(config_file.c_str(), 0, CV_STORAGE_READ);

   track_features_algorithm = cvReadIntByName(fs, 0, "track_features_algorithm", track_features_algorithm);
   nFeatures = cvReadIntByName(fs, 0, "nFeatures", nFeatures);
   max_residue = cvReadRealByName(fs, 0, "max_residue", max_residue);
   log_files_directory = cvReadStringByName(fs, 0, "log_files_directory", log_files_directory.c_str());

   cvReleaseFileStorage(&fs);
}
#endif


int main (int argc, char** argv) {
#if 0
  // we need the path name to 2010_03_09_drive_0019 as input argument
  if (argc<2) {
    cerr << "Usage: ./viso2 path/to/sequence/2010_03_09_drive_0019" << endl;
    return 1;
  }

  // sequence directory
  string dir = argv[1];
#else
	string dir = "D:/libviso2/2010_03_09_drive_0019";
#endif

  SOCKET ConnectSocket = init_tcp_client();

  // set most important visual odometry parameters
  // for a full parameter list, look at: viso_stereo.h
  VisualOdometryStereo::parameters param;
  
  // calibration parameters for sequence 2010_03_09_drive_0019 
  param.calib.f  = 645.24; // focal length in pixels
  param.calib.cu = 635.96; // principal point (u-coordinate) in pixels
  param.calib.cv = 194.13; // principal point (v-coordinate) in pixels
  param.base     = 0.5707; // baseline in meters
  
  // init visual odometry
  VisualOdometryStereo viso;
  viso.set_param(&param);
  
  // current pose (this matrix transforms a point from the current
  // frame's camera coordinates to the first frame's camera coordinates)
  Matrix pose = Matrix::eye(4);
  initViz(viz_);
  std::vector<double> tr_delta;
  // loop through all frames i=0:372
  for (int32_t i=0; i<373; i++) {

    // input file names
    char base_name[256]; sprintf(base_name,"%06d.png",i);
    string left_img_file_name  = dir + "/I1_" + base_name;
    string right_img_file_name = dir + "/I2_" + base_name;

	//printf("left_img_file_name=%s\n", left_img_file_name.c_str());
	//printf("right_img_file_name=%s\n", right_img_file_name.c_str());
    
    // catch image read/write errors here
    try {
#if 0
      // load left and right input image
      png::image< png::gray_pixel > left_img(left_img_file_name);
      png::image< png::gray_pixel > right_img(right_img_file_name);

      // image dimensions
      int32_t width  = left_img.get_width();
      int32_t height = left_img.get_height();
#else
		cv::Mat left_img = cv::imread(left_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);
		cv::Mat right_img = cv::imread(right_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);

      // image dimensions
      int32_t width  = left_img.cols;
      int32_t height = left_img.rows;

      printf("width=%d, height=%d\n", width, height);

#endif

      // convert input images to uint8_t buffer
      uint8_t* left_img_data  = (uint8_t*)malloc(width*height*sizeof(uint8_t));
      uint8_t* right_img_data = (uint8_t*)malloc(width*height*sizeof(uint8_t));
      int32_t k=0;
      for (int32_t v=0; v<height; v++) {
        for (int32_t u=0; u<width; u++) {
#if 0
          left_img_data[k]  = left_img.get_pixel(u,v);
          right_img_data[k] = right_img.get_pixel(u,v);
#else
          left_img_data[k]  = left_img.at<uint8_t>(cv::Point(u,v));
          right_img_data[k] = right_img.at<uint8_t>(cv::Point(u,v));
#endif
          k++;
        }
      }

      // status
      cout << "Processing: Frame: " << i;
      
      // compute visual odometry
      int32_t dims[] = {width,height,width};
      if (viso.process(left_img_data,right_img_data,dims)) {
      
        // on success, update current pose
        pose = pose * Matrix::inv(viso.getMotion());
      
        // output some statistics
        double num_matches = viso.getNumberOfMatches();
        double num_inliers = viso.getNumberOfInliers();
        cout << ", Matches: " << num_matches;
        cout << ", Inliers: " << 100.0*num_inliers/num_matches << " %" << ", Current pose: " << endl;
        cout << pose << endl << endl;
        updateViz(viz_, pose);

        tr_delta = viso.getTransformationDelta();

        cout << "tr_delta:" << endl;
        for (int j = 0; j < tr_delta.size(); ++j)
           cout << tr_delta[j] << " ";
        cout << endl << endl;

        update_tcp_client(ConnectSocket, tr_delta);
        //tcp_receive(ConnectSocket);

      } else {
        cout << " ... failed!" << endl;
      }

      imshow("this is you, smile! :)", left_img);
      if( cv::waitKey(1) == 27 ) break; // stop capturing by pressing ESC 

      // release uint8_t buffers
      free(left_img_data);
      free(right_img_data);

    // catch image read errors here
    } catch (...) {
      cerr << "ERROR: Couldn't read input files!" << endl;
      tcp_close(ConnectSocket);
      return 1;
    }
  }

  tcp_close(ConnectSocket);

  // output
  cout << "Demo complete! Exiting ..." << endl;

  // exit
  return 0;
}

