#include "dllapi.h"

#include <stdarg.h>
#include <Windows.h>
void log(const char* format, ...);

extern "C"
{
	LIBVISO2_API CLibVisoApi* CreateLibVisoApi()
	{
		return new CLibVisoApi();
	}

	LIBVISO2_API void Init(CLibVisoApi* api)
	{
		api->Init();
	}
	LIBVISO2_API bool CalcCurentMatrix(CLibVisoApi* api)
	{
		return api->CalcCurentMatrix();
	}

	LIBVISO2_API void DeleteLibVisoApi(CLibVisoApi* api)
	{
		delete api;
	}

	LIBVISO2_API _FLOAT* GetPoseMatrix(CLibVisoApi* api) 
	{
		return api->pose.val[0];
	}
	LIBVISO2_API _FLOAT* GetMotionMatrix(CLibVisoApi* api) 
	{
		return api->motion.val[0];
	}
	LIBVISO2_API _FLOAT* GetInvMotionMatrix(CLibVisoApi* api) 
	{
		return api->inv_motion.val[0];
	}

	LIBVISO2_API _FLOAT* GetTranslationDelta(CLibVisoApi* api) 
	{
		return &api->tr_delta[0];
	}

}

CLibVisoApi::CLibVisoApi()
	: pose(Matrix::eye(4))
	, hEvent (CreateEvent(0, TRUE, TRUE, 0))
{
}

CLibVisoApi::~CLibVisoApi()
{
	free(left_img_data);
}

bool CLibVisoApi::Init()
{
	// calibration parameters for sequence 2010_03_09_drive_0019 
	param.calib.f  = 645.24; // focal length in pixels
	param.calib.cu = 635.96; // principal point (u-coordinate) in pixels
	param.calib.cv = 194.13; // principal point (v-coordinate) in pixels
#if !USE_CAM
	param.base     = 0.5707; // baseline in meters
#endif
	viso.set_param(&param);

#if USE_CAM
	//if(!cap.open(1))
	//	return false;
	int ndevice = 9;
  //printf("Enter device number\n");
  //std::cin >> ndevice;
  while(!cap.open(ndevice))
  {
    --ndevice;
	if (ndevice < -1)
		return false;
  }


#else
	dir = "D:/Work/odometry/libviso2/2010_03_09_drive_0019";
	iFrame = 0;
#endif

	SetEvent(hEvent);
#if USE_CAM
	width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
	height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
#else
	char base_name[256]; sprintf(base_name,"%06d.png", iFrame);
	std::string left_img_file_name  = dir + "/I1_" + base_name;
	left_img = cv::imread(left_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);
	//cv::Mat right_img = cv::imread(right_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);

	// image dimensions
	width  = left_img.cols;
	height = left_img.rows;
#endif
	left_img_data  = (uint8_t*)malloc(width*height*sizeof(uint8_t));
	right_img_data  = (uint8_t*)malloc(width*height*sizeof(uint8_t));
	return true;
}




bool CLibVisoApi::CalcCurentMatrix()
{
	if (WAIT_OBJECT_0 != WaitForSingleObject(hEvent, 0)){
		log("WAIT_OBJECT_0 != WaitForSingleObject");
		return false;
	}
	ResetEvent(hEvent);
#if USE_CAM
	cap >> left_img;
#else
	// input file names
	char base_name[256]; sprintf(base_name,"%06d.png", iFrame);
	std::string left_img_file_name  = dir + "/I1_" + base_name;
	std::string right_img_file_name = dir + "/I2_" + base_name;

	left_img = cv::imread(left_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);
	right_img = cv::imread(right_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);
	log(left_img_file_name.c_str());
	log(right_img_file_name.c_str());
	++iFrame;
#endif

	if( left_img.empty() )
	{
		log("CalcCurentMatrix empty");
		SetEvent(hEvent);
		return false; // end of video stream
	}
	int32_t k = 0;

	for (int32_t v=0; v<height; v++) {
		for (int32_t u=0; u<width; u++) {
			left_img_data[k]  = left_img.at<uint8_t>(cv::Point(u,v));
			right_img_data[k] = right_img.at<uint8_t>(cv::Point(u,v));
			k++;
		}
	}

	bool result = true;

	try
	{
		// compute visual odometry
		int32_t dims[] = {width,height,width};
		bool replace = false;
		bool skip_matrix_calculation = false;
#if USE_CAM
		if (viso.process(left_img_data, dims, replace, skip_matrix_calculation)) {
#else
		if (viso.process(left_img_data, right_img_data, dims, replace, skip_matrix_calculation)) {
#endif
			tr_delta = viso.getTransformationDelta();

			std::stringstream ss;
			ss  << "   "
				<< tr_delta[0] << " " 
				<< tr_delta[1] << " "
				<< tr_delta[2] << " " 

				<< tr_delta[3] << " " 
				<< tr_delta[4] << " "
				<< tr_delta[5] << " " 

				<< std::endl << std::endl;
			log(ss.str().c_str());

			if (skip_matrix_calculation)
			{
			}
			else
			{

				motion = viso.getMotion();
				inv_motion =  Matrix::inv(motion);

				// on success, update current pose
				pose = pose * inv_motion;

				// output some statistics
				double num_matches = viso.getNumberOfMatches();
				double num_inliers = viso.getNumberOfInliers();
				std::stringstream ss;
				ss << "Matches: " << num_matches;
				ss << ", Inliers: " << 100.0*num_inliers/num_matches << " %" << ", Current motion: " << std::endl;
				ss << motion << std::endl << std::endl;
				ss << "Current inv_motion: " << std::endl;
				ss << inv_motion << std::endl << std::endl;
				ss << "Current pose: " << std::endl;
				ss << pose << std::endl << std::endl;
				log(ss.str().c_str());
			}

		} else {
			//cout << " ... failed!" << endl;
			log(" ... failed!");
			result = false;
		}
	}
	catch(std::string& err)
	{
		log(err.c_str());
		result = false;
	}
	catch(...)
	{
		log("catch(...)");
		result = false;
	}

	cv::imshow("this is you, smile! :)", left_img);
	cv::imshow("right_img :)", right_img);

	SetEvent(hEvent);

	log("CalcCurentMatrix result = %d", result);

	return result;

}
