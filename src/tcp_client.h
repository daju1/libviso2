#include <windows.h>

SOCKET init_tcp_client();
int update_tcp_client(SOCKET ConnectSocket, const std::vector<double>& tr_delta);
int tcp_receive(SOCKET ConnectSocket);
void tcp_close(SOCKET ConnectSocket);
