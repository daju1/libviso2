
#include <iostream>
#include <string>
#include <vector>
//#include <stdint.h>

#include <viso_mono.h>
#include <stdarg.h>

#if 0
#include <png++/png.hpp>
#else
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2\opencv.hpp>
#include <opencv2/viz.hpp>
#endif

cv::viz::Viz3d viz_("abc");
#if USE_VIZ
void initViz(cv::viz::Viz3d& viz_);
void updateViz(cv::viz::Viz3d viz_, const Matrix& m_pose);
#endif
#include "opencv2/opencv.hpp"
using namespace cv;
int main_(int argc, char** argv)
{
    VideoCapture cap;
    // open the default camera, use something different from 0 otherwise;
    // Check VideoCapture documentation.
    if(!cap.open(0))
        return 0;
    for(;;)
    {
          Mat frame;
          cap >> frame;
          if( frame.empty() ) break; // end of video stream
          imshow("this is you, smile! :)", frame);
          if( waitKey(1) == 27 ) break; // stop capturing by pressing ESC 
    }
    // the camera will be closed automatically upon exit
    // cap.close();
    return 0;
}

#define HAVE_GETSYSTEMTIMEASFILETIME 1
#include <Windows.h>

#if 0
struct timeval {
    long    tv_sec;         /* seconds */
    long    tv_usec;        /* and microseconds */
};
#endif
timeval end_time;
timeval start_time;



int64_t inline static av_gettime(void)
{
#if HAVE_GETTIMEOFDAY
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (int64_t)tv.tv_sec * 1000000 + tv.tv_usec;
#elif HAVE_GETSYSTEMTIMEASFILETIME
    FILETIME ft;
    int64_t t;
    GetSystemTimeAsFileTime(&ft);
    t = (int64_t)ft.dwHighDateTime << 32 | ft.dwLowDateTime;
    return t / 10 - 11644473600000000; /* Jan 1, 1601 */
#else
    return -1;
#endif
}
int inline static __cdecl gettimeofday(struct timeval * /*__restrict__*/ tv,
    void * /*__restrict__*/ /*	tzp (unused) */)
{
    int64_t av_time = av_gettime();
    tv->tv_sec = av_time / 1000000;
    tv->tv_usec = av_time % 1000000;
    return 0;
}

inline void start_timer()
{
    gettimeofday(&start_time, NULL);
}

inline double stop_timer()
{
    gettimeofday(&end_time, NULL);
    long seconds = end_time.tv_sec - start_time.tv_sec;
    long useconds = end_time.tv_usec - start_time.tv_usec;
    double time_ = ((seconds)+useconds*0.000001)/* + accumulated_*/;
    //accumulated_ = time_;
    return time_;
}
using namespace std;
#define USE_CAM 0
int main (int argc, char** argv) {
#if USE_CAM
    VideoCapture cap;
    if(!cap.open(0))
        return 0;
#else
#if 0
    // we need the path name to 2010_03_09_drive_0019 as input argument
    if (argc<2) {
        cerr << "Usage: ./viso2 path/to/sequence/2010_03_09_drive_0019" << endl;
        return 1;
    }

    // sequence directory
    string dir = argv[1];
#else
    string dir = "D:/libviso2/2010_03_09_drive_0019";
#endif
#endif

    start_timer();
 
    // set most important visual odometry parameters
    // for a full parameter list, look at: viso_stereo.h
    VisualOdometryMono::parameters param;

    // calibration parameters for sequence 2010_03_09_drive_0019 
    param.calib.f  = 645.24; // focal length in pixels
    param.calib.cu = 635.96; // principal point (u-coordinate) in pixels
    param.calib.cv = 194.13; // principal point (v-coordinate) in pixels
    //param.base     = 0.5707; // baseline in meters

    // init visual odometry
    VisualOdometryMono viso;
    viso.set_param(&param);

    // current pose (this matrix transforms a point from the current
    // frame's camera coordinates to the first frame's camera coordinates)
    Matrix pose = Matrix::eye(4);
#if USE_VIZ
    initViz(viz_);
#endif
#if USE_CAM
    for (int32_t i=0;; i++) {
#else
    // loop through all frames i=0:372
    for (int32_t i=0; i<1249; i++) {

        // input file names
        char base_name[256]; sprintf(base_name,"%06d.png",i);
        string left_img_file_name  = dir + "/I1_" + base_name;
        //string right_img_file_name = dir + "/I2_" + base_name;
#endif
        //printf("left_img_file_name=%s\n", left_img_file_name.c_str());
        //printf("right_img_file_name=%s\n", right_img_file_name.c_str());

        // catch image read/write errors here
        try {
#if 0
            // load left and right input image
            png::image< png::gray_pixel > left_img(left_img_file_name);
            //png::image< png::gray_pixel > right_img(right_img_file_name);

            // image dimensions
            int32_t width  = left_img.get_width();
            int32_t height = left_img.get_height();
#else
#if USE_CAM
            //grab and retrieve each frames of the video sequentially 
            Mat left_img;
            cap >> left_img;
            if( left_img.empty() ) break; // end of video stream
#else
            cv::Mat left_img = cv::imread(left_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);
            //cv::Mat right_img = cv::imread(right_img_file_name, CV_LOAD_IMAGE_GRAYSCALE);
#endif
            // image dimensions
            int32_t width  = left_img.cols;
            int32_t height = left_img.rows;

            //printf("width=%d, height=%d\n", width, height);

#endif

            // convert input images to uint8_t buffer
            uint8_t* left_img_data  = (uint8_t*)malloc(width*height*sizeof(uint8_t));
            uint8_t* right_img_data = (uint8_t*)malloc(width*height*sizeof(uint8_t));
            int32_t k=0;
            for (int32_t v=0; v<height; v++) {
                for (int32_t u=0; u<width; u++) {
#if 0
                    left_img_data[k]  = left_img.get_pixel(u,v);
                    //right_img_data[k] = right_img.get_pixel(u,v);
#else
                    left_img_data[k]  = left_img.at<uint8_t>(cv::Point(u,v));
                    //right_img_data[k] = right_img.at<uint8_t>(cv::Point(u,v));
#endif
                    k++;
                }
            }

            // status
            cout << "Processing: Frame: " << i;

            // compute visual odometry
            int32_t dims[] = {width,height,width};
            if (viso.process(left_img_data, dims)) {

                // on success, update current pose
                pose = pose * Matrix::inv(viso.getMotion());

                // output some statistics
                double num_matches = viso.getNumberOfMatches();
                double num_inliers = viso.getNumberOfInliers();
                cout << ", Matches: " << num_matches;
                cout << ", Inliers: " << 100.0*num_inliers/num_matches << " %" << ", Current pose: " << endl;
                cout << pose << endl << endl;
#if USE_VIZ
                updateViz(viz_, pose);
#endif

            } else {
                cout << " ... failed!" << endl;
            }

            imshow("this is you, smile! :)", left_img);
            if( waitKey(1) == 27 ) break; // stop capturing by pressing ESC 


            // release uint8_t buffers
            free(left_img_data);
            free(right_img_data);

            // catch image read errors here
        } catch (...) {
            cerr << "ERROR: Couldn't read input files!" << endl;
            double time = stop_timer();
            cout << "time = " << time << endl;
            return 1;
        }
    }

    double time = stop_timer();
    cout << "time = " << time << endl;

    // output
    cout << "Demo complete! Exiting ..." << endl;

    // exit
    return 0;
}

