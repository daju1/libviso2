﻿using System;
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class CameraScript : MonoBehaviour {

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern int Add(int a, int b);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern IntPtr CreateLibVisoApi();

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern void DeleteLibVisoApi(IntPtr api);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern void Init(IntPtr api);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern bool CalcCurentMatrix(IntPtr api);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern double GetMatrixElement(IntPtr api, int i, int j);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern IntPtr GetPoseMatrix(IntPtr api);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern IntPtr GetMotionMatrix(IntPtr api);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern IntPtr GetInvMotionMatrix(IntPtr api);

    [DllImport("libviso2.dll", CharSet = CharSet.Unicode)]
    static extern IntPtr GetTranslationDelta(IntPtr api);




    UnityEngine.Camera cam;
    private IntPtr api = CreateLibVisoApi();
    //Matrix4x4 leftMatrix = eye();
    //Matrix4x4 rightMatrix = eye();

    //double[] pose = new double[16];
    double[] tr_delta = new double[6];

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
        Init(api);
    }

    static Matrix4x4 eye()
    {
        Matrix4x4 m = new Matrix4x4();
        m.m00 = 1.0F;
        m.m11 = 1.0F;
        m.m22 = 1.0F;
        m.m33 = 1.0F;

        return m;
    }

    /*float getPoseMatrixVal(int i, int j)
    {
        return (float)pose[4 * i + j];
    }*/

    // Update is called once per frame
    void Update()
    {
        if (CalcCurentMatrix(api))
        {

            IntPtr tr_src = GetTranslationDelta(api);
            Marshal.Copy(tr_src, tr_delta, 0, 6);

            cam.transform.Rotate(-(float)tr_delta[0] * 180 / Mathf.PI, -(float)tr_delta[1] * 180 / Mathf.PI, -(float)tr_delta[2] * 180 / Mathf.PI);
            cam.transform.Translate(-(float)tr_delta[3], -(float)tr_delta[4], -(float)tr_delta[5]);


            //IntPtr src = GetPoseMatrix(api);
            //Marshal.Copy(src, pose, 0,  4*4);
            /*
            for (int i = 0; i < 4; i++)
            {
                Debug.Log(string.Format("{0} {1} {2} {3}"
                    , pose[i * 4 + 0]
                    , pose[i * 4 + 1]
                    , pose[i * 4 + 2]
                    , pose[i * 4 + 3]
                    ));
            }

            Matrix4x4 p = cam.projectionMatrix;
            p.m00 = getPoseMatrixVal(0, 0);
            p.m01 = getPoseMatrixVal(0, 1);
            p.m02 = getPoseMatrixVal(0, 2);
            p.m03 = getPoseMatrixVal(0, 3);

            p.m10 = getPoseMatrixVal(1, 0);
            p.m11 = getPoseMatrixVal(1, 1);
            p.m12 = getPoseMatrixVal(1, 2);
            p.m13 = getPoseMatrixVal(1, 3);

            p.m20 = getPoseMatrixVal(2, 0);
            p.m21 = getPoseMatrixVal(2, 1);
            p.m22 = getPoseMatrixVal(2, 2);
            p.m23 = getPoseMatrixVal(2, 3);

            p.m30 = getPoseMatrixVal(3, 0);
            p.m31 = getPoseMatrixVal(3, 1);
            p.m32 = getPoseMatrixVal(3, 2);
            p.m33 = getPoseMatrixVal(3, 3);

            cam.projectionMatrix = p;*/
        }



        /*
        Matrix4x4 m = cam.transform.worldToLocalMatrix;

        Debug.Log(string.Format("Hello {0}", Add(1,5).ToString()));



        Debug.Log("Hello", gameObject);
        //Debug.Log("cam.transform", cam.transform);
        //Debug.Log("cam.transform.worldToLocalMatrix", cam.transform);
        Debug.Log(string.Format("{0}", cam.transform.tag));
        Debug.Log(string.Format("{0} {1} {2} {3}", m.m00, m.m01, m.m02, m.m03));
        Debug.Log(string.Format("{0} {1} {2} {3}", m.m10, m.m11, m.m12, m.m13));
        Debug.Log(string.Format("{0} {1} {2} {3}", m.m20, m.m21, m.m22, m.m23));
        Debug.Log(string.Format("{0} {1} {2} {3}", m.m30, m.m31, m.m32, m.m33));

        Debug.Log(string.Format("eulerAngles {0} {1} {2}"
            , cam.transform.eulerAngles.x
            , cam.transform.eulerAngles.y
            , cam.transform.eulerAngles.z
            ));
        Debug.Log(string.Format("forward {0} {1} {2}"
             , cam.transform.forward.x
             , cam.transform.forward.y
             , cam.transform.forward.z
             ));

        Debug.Log(string.Format("localEulerAngles {0} {1} {2}"
            , cam.transform.localEulerAngles.x
            , cam.transform.localEulerAngles.y
            , cam.transform.localEulerAngles.z
            ));
        Debug.Log(string.Format("localPosition {0} {1} {2}"
             , cam.transform.localPosition.x
             , cam.transform.localPosition.y
             , cam.transform.localPosition.z
             ));
        Debug.Log(string.Format("localRotation localRotation {0} {1} {2}"
              , cam.transform.localRotation.x
              , cam.transform.localRotation.y
              , cam.transform.localRotation.z
              ));
        Debug.Log(string.Format("localScale {0} {1} {2}"
          , cam.transform.localScale.x
          , cam.transform.localScale.y
          , cam.transform.localScale.z
          ));
        Debug.Log(string.Format("position {0} {1} {2}"
          , cam.transform.position.x
          , cam.transform.position.y
          , cam.transform.position.z
          ));
        Debug.Log(string.Format("right {0} {1} {2}"
          , cam.transform.right.x
          , cam.transform.right.y
          , cam.transform.right.z
          ));
        Debug.Log(string.Format("rotation {0} {1} {2}"
          , cam.transform.rotation.x
          , cam.transform.rotation.y
          , cam.transform.rotation.z
          ));
        Debug.Log(string.Format("up {0} {1} {2}"
          , cam.transform.up.x
          , cam.transform.up.y
          , cam.transform.up.z
          ));

        cam.transform.Rotate(1.0F, 1.0F, 1.0F);
        cam.transform.Translate(1.0F, 1.0F, 1.0F);


        // cam.transform.worldToLocalMatrix.m23 += 1.0F;

        leftMatrix.m12 += 1.0F;
        rightMatrix.m12 += 1.0F;
        //cam.SetStereoViewMatrices(leftMatrix, rightMatrix);
        */
    }
}
