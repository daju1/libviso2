# project
cmake_minimum_required (VERSION 2.6)
project (libviso2)
find_package(OpenCV REQUIRED)

add_definitions(-DBOOST_ALL_NO_LIB)
add_definitions(-DBOOST_ASIO_SEPARATE_COMPILATION )

set(Boost_USE_STATIC_LIBS       OFF)
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)

FIND_PACKAGE(Boost REQUIRED COMPONENTS thread system regex serialization)


# directories
set (LIBVISO2_SRC_DIR src)

# include directory
include_directories(
  ${LIBVISO2_SRC_DIR}
  ${Boost_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
)

# use sse3 instruction set
SET(CMAKE_CXX_FLAGS "-msse3")

# sources
FILE(GLOB LIBVISO2_SRC_FILES "src/*.cpp")
FILE(GLOB LIBVISO2_H_FILES "src/*.h")

# make release version
set(CMAKE_BUILD_TYPE Release)

# demo program
add_executable(viso2_stereo
	"src/filter.cpp"
	"src/matcher.cpp"
	"src/matrix.cpp"
	"src/reconstruction.cpp"
	"src/triangle.cpp"
	"src/viso.cpp"
	"src/viso_mono.cpp"	
	"src/viso_stereo.cpp"
	"src/demo.cpp"
	"src/log.cpp"
	"src/viz.cpp"
	"src/tcp_client.cpp"

	 ${LIBVISO2_H_FILES}
)
target_link_libraries (
  viso2_stereo
  ${OpenCV_LIBRARIES}
  ${Boost_LIBRARIES}
)

# demo program
add_executable(viso2_mono
	"src/filter.cpp"
	"src/matcher.cpp"
	"src/matrix.cpp"
	"src/reconstruction.cpp"
	"src/triangle.cpp"
	"src/viso.cpp"
	"src/viso_mono.cpp"	
	"src/viso_stereo.cpp"
	"src/demo_mono.cpp"
	"src/log.cpp"
	"src/viz.cpp"

	 ${LIBVISO2_H_FILES}
)
target_link_libraries (viso2_mono ${OpenCV_LIBRARIES})

add_library(libviso2 SHARED 
	"src/filter.cpp"
	"src/matcher.cpp"
	"src/matrix.cpp"
	"src/reconstruction.cpp"
	"src/triangle.cpp"
	"src/viso.cpp"
	"src/viso_mono.cpp"	
	"src/viso_stereo.cpp"
	"src/dllapi.cpp"
	"src/log.cpp"

	 ${LIBVISO2_H_FILES}
)
target_link_libraries (libviso2 ${OpenCV_LIBRARIES})

set_target_properties(libviso2
PROPERTIES
OUTPUT_DIRECTORY "C:/Users/Public/Documents/Unity Projects/Standard Assets Example Project/")

